package com.example.activity7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private static final String FILE_NAME = "myFile";
    private TextView welcomeUsername;
    private Button btnLogout, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        welcomeUsername = findViewById(R.id.welcomeUsername);
        btnLogout = findViewById(R.id.btnLogout);
        button2 = findViewById(R.id.button2);

        SharedPreferences sharedPreferences = getSharedPreferences(FILE_NAME, MODE_PRIVATE);
        String username = sharedPreferences.getString("username","Data not found");

        welcomeUsername.setText("Hello " + username);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,LocationActivity.class);
                startActivity(intent);
            }
        });

    }
}